﻿FROM nginxinc/nginx-unprivileged:1.25-alpine3.18

#See https://hub.docker.com/r/nginxinc/nginx-unprivileged/tags?page=1&name=alpine
ARG ALPINE_VERSION=v3.18
#See https://opentelemetry.io/blog/2022/instrument-nginx/ && https://github.com/open-telemetry/opentelemetry-cpp-contrib/releases/
ARG OPEN_TELEMETRY_WEBSERVER_SDK_VERSION=v1.0.4
ARG OPEN_TELEMETRY_WEBSERVER_SDK=opentelemetry-webserver-sdk-x64-linux.tgz

RUN mkdir -p /opt
WORKDIR /opt

USER root

RUN apk add \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      ca-certificates unzip
ADD https://github.com/open-telemetry/opentelemetry-cpp-contrib/releases/download/webserver/${OPEN_TELEMETRY_WEBSERVER_SDK_VERSION}/${OPEN_TELEMETRY_WEBSERVER_SDK} ${OPEN_TELEMETRY_WEBSERVER_SDK}.zip
RUN unzip ${OPEN_TELEMETRY_WEBSERVER_SDK}.zip; tar xvfz ${OPEN_TELEMETRY_WEBSERVER_SDK}
WORKDIR /opt/opentelemetry-webserver-sdk
RUN ./install.sh
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/opentelemetry-webserver-sdk/sdk_lib/lib
RUN echo "load_module /opt/opentelemetry-webserver-sdk/WebServerModule/Nginx/1.25.3/ngx_http_opentelemetry_module.so;\n$(cat /etc/nginx/nginx.conf)" > /etc/nginx/nginx.conf
COPY opentelemetry_module.conf /etc/nginx/conf.d

RUN apk del \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      unzip

ENV TZ=UTC
